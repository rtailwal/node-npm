module.exports = xformettime;

function xformettime (input){
	this.time = typeof input.time !== "undefined" ? input.time : '' 
}

xformettime.prototype.getElement = function() {

	if(this.time == null || this.time == "") {
		return "";
	} else {
		var d = new Date(this.time);
		var date = d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
		return date;
	}
}