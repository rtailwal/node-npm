module.exports = xCheckbox;

function xCheckbox (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.checked = typeof input.checked !== "undefined" ? input.checked : false,
	this.switch_x = typeof input.switch_x !== "undefined" ? input.switch_x : false,
	this.align = typeof input.align !== "undefined" ? input.align : false,
	this.inline = typeof input.inline !== "undefined" ? input.inline : false,
	this.form_group = typeof input.form_group !== "undefined" ? input.form_group : true,
	this.required = typeof input.required !== "undefined" ? input.required : false
};

xCheckbox.prototype.getElement = function() {
	checked_output = ""
	switchery_div = ""
	switchery_input = ""
	align_class = ""
	inline_class = ""
	form_group_start = ""
	form_group_end = ""
	required_output = ""
	output = ""

	checked_output = this.checked == true ? 'checked="checked"' : '';
	switchery_div = this.switch_x == true ? 'checkbox-switchery' : '';
	switchery_input = this.switch_x == true ? 'switch' : '';
	align_class = this.align == "left" ? 'pull-left' : '';
	align_class = this.align == "right" ? 'pull-right' : '';
	inline_class = this.inline == true ? 'inline' : '';
	form_group_start = this.form_group == true ? '<div class="form-group">' : '';
	form_group_end = this.form_group == true ? '</div>' : '';
	required_output = this.required == true ? "data-parsley-mincheck='1'" : '';
	output += form_group_start
	output += '<div class="checkbox '+switchery_div+'"'
	output += '<label class="'+align_class+' '+ inline_class+'>'
	output += '<input type="hidden" id="'+this.id+'" name="post['+this.name+']" value="0">'
	output += '<input type="checkbox" class="'+switchery_input+'" id="'+this.id+'" name="post['+this.name+']" value="1" '+checked_output+' data-toggle="checkbox" '+required_output+'/>'
	output += this.label+'</label>'
	output += '</div>'
	output += form_group_end
	return output;
}