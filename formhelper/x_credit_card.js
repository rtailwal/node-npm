module.exports = xCreditCard;

function xCreditCard (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.maxlength = typeof input.maxlength !== "undefined" ? input.maxlength : 16,
	this.mask = typeof input.mask !== "undefined" ? input.mask : ""
};

xCreditCard.prototype.getElement = function() {
	required_output = '';
	required_tag = '';
	label = '';
	mask = '';
	if (this.required){
		if (this.label != "")
      		required_output = '<sup class="text-danger">*</sup>'
    	else
      		required_output = ''
    	required_tag = 'required="required"'
	}else{
  		required_output = ""
    	required_tag = ""
  	}

	if (this.label != ""){
		label = "<label>"+this.label+required_output+" <small>(Numbers Only)</small></label>"
	}
    
	if (this.mask != ""){
		mask = "data-masked-input='"+this.mask+"'"
	}
	
  	if (this.maxlength != ""){
  		maxlength = "maxlength='"+this.maxlength+"'"
  	}   

	output ='<div class="form-group">'
	output += label
	output += '<input type="number" pattern="[0-9]{13,16}" class="form-control" id="'+this.id+'" name="post['+this.name+']" value="'+this.value+'" '+required_tag+' data-parsley-type="integer" data-parsley-length="[13, 16]" placeholder="'+this.placeholder+'"  '+mask+' '+maxlength+'  />'
	output += '</div>'
	return output;
}