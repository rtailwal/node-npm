module.exports = xdate;

function xdate (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : ''
}

xdate.prototype.getElement = function() {
	required_output = '';
	required_tag = '';

	if(this.required) {
		if(this.label) {
			required_output = "<sup class='text-danger'>*</sup>";
		} else {
			required_output = "";
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}
	if(this.label != "") {
		label = "<label for='"+this.id+"' class='control-label'>"+this.label+"</label>"	
	}

	output = `<div class="form-group">
		<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		<input type="text" class="form-control datetimepicker" id="${this.id}" name="post[${this.name}]" value="${this.value}" ${required_tag} placeholder="${this.placeholder}"  />
		</div>
		</div>`;

	return output;
}