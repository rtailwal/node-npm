module.exports = xein;

function xein (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.maxlength = typeof input.maxlength !== "undefined" ? input.maxlength : "10",
	this.mask = typeof input.mask !== "undefined" ? input.mask : "99-9999999"
};

xein.prototype.getElement = function() {
	required_output = '';
	required_tag = '';
	if (this.required){
		if (this.label != "")
      		required_output = '<sup class="text-danger">*</sup>'
    	else
      		required_output = ''
    	required_tag = 'required="required"'
	}else{
  		required_output = ""
    	required_tag = ""
  	}

	if (this.label != ""){
		label = "<label>"+this.label+required_output+"</label>"
	}

	if (this.mask != ""){
		mask = "data-masked-input='"+this.mask+"'"
	}

  	if (this.maxlength != ""){
  		maxlength = "maxlength='"+this.maxlength+"'"
  	}

	output ='<div class="form-group">'
	output += label
	output += '<input type="text" class="form-control ein" id="'+this.id+'" name="post['+this.name+']" value="'+this.value+'" '+required_tag+' data-parsley-minlength="10" placeholder="'+this.placeholder+'" '+this.mask+' '+this.maxlength+'} />'
	output += '</div>'
	return output;
}