module.exports = xfile;

function xfile (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.css = typeof input.css !== "undefined" ? input.css : ''
}

xfile.prototype.getElement = function() {
	required_output = '';
	required_tag = '';

	if(this.required) {
		if(this.label) {
			required_output = "<sup class='text-danger'>*</sup>";
		} else {
			required_output = "";
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}
	if(this.label != "") {
		label = "<label>"+this.label+"</label>"
	}

	output = `<div class="form-group">
		${this.label} ${required_output}
		<input type="file" class="form-control file ${required_output}" id="${this.id}" name="filename" value="${this.value}" ${required_tag} placeholder="${this.placeholder}" />
		</div>`;

	return output;
}