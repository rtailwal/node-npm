module.exports = xhidden;

function xhidden (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : ''
}

xhidden.prototype.getElement = function() {

	output = `<input type="hidden" id="${this.id}" name="post[${this.name}]" value="${this.value}"/>`;

	return output;
}