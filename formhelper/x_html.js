module.exports = xhtml;

function xhtml (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false
}

xhtml.prototype.getElement = function() {
	required_output = '';
	required_tag = '';

	if(this.required) {
		if(this.label) {
			required_output = "<sup class='text-danger'>*</sup>";
		} else {
			required_output = "";
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}
	if(this.label != "") {
		this.label = "<label for='"+this.id+"' class='control-label'>"+this.label+"</label>"	
	}

	output = `<div class="form-group">
		     ${this.label}
		     <textarea name="post[${this.name}]" id="${this.id}" rows="4" class="form-control summernote">${this.value}</textarea>
		</div>`;

	return output;
}