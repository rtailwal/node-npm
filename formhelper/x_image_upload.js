module.exports = ximageupload;

function ximageupload (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.css = typeof input.css !== "undefined" ? input.css : '',
	this.height = typeof input.height !== "undefined" ? input.height : '',
	this.size = typeof input.size !== "undefined" ? input.size : '',
	this.default = typeof input.default !== "undefined" ? input.default : ''
}

ximageupload.prototype.getElement = function() {
	required_output = '';
	required_tag = '';

	if(this.required && this.label != "") {
		required_output = '<sup class="text-danger">*</sup>';
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}
	if(this.height == "") {
		this.height = "";
	} else {
		this.height="data-height='${this.height}'";
	}

	output = `<div class="form-group">
		     <label>${this.label}</label>
		     <input type="file" class="form-control file dropify" data-default-file="${this.value}" data-max-file-size="${this.size}" id="${this.id}" name="filename" value="${this.value}" ${required_tag} height="${this.height}"/>
		</div>`;

	return output;
}