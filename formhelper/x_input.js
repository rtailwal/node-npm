module.exports = xinput;

function xinput (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.mask = typeof input.mask !== "undefined" ? input.mask : '',
	this.maxlength = typeof input.maxlength !== "undefined" ? input.maxlength : '',
	this.css = typeof input.css !== "undefined" ? input.css : ''
}

xinput.prototype.getElement = function() {
	required_output = '';
	required_tag = '';

	if(this.required) {
		if(this.label != "") {
			required_output = '<sup class="text-danger">*</sup>';
		} else {
			required_output = '';
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}

	if(this.label != "") {
		this.label = `<label>${this.label}</label>`
	}

	if(this.maxlength != "") {
		this.maxlength = "maxlength ="+this.maxlength;
	}

	if(this.mask != "") {
		this.mask = "data-masked-input="+this.mask;
	}

	output = `<div class="form-group">
		     ${this.label} ${required_output}
		     <input type="text" class="form-control ${this.css}" id="${this.id}" name="post[${this.name}]" value="${this.value}" placeholder="${this.placeholder}" ${required_tag} ${this.mask} ${this.maxlength} />
			</div>`;

	return output;
}