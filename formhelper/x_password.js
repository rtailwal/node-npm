module.exports = xpassword;

function xpassword (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : ''
}

xpassword.prototype.getElement = function() {
	required_output = '';
	required_tag = '';

	if(this.required) {
		if(this.label != "") {
			required_output = "<sup class='text-danger'>*</sup>";
		} else {
			required_output = "";
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}

	if(this.label != "") {
		this.label = "<label>"+this.label+"</label>";
	}

	output = `<div class="form-group">
			  ${this.label}${required_output}
			  <input type="password" class="form-control" id="${this.id}" name="post[${this.name}]" value="${this.value}" ${required_tag} data-parsley-length="[6, 34]" placeholder="${this.placeholder}" />
			  </div>`;

	return output;
}