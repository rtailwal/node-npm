module.exports = xphone;

function xphone (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.mask = typeof input.mask !== "undefined" ? input.mask : '(999) 999-9999 x99999',
	this.maxlength = typeof input.maxlength !== "undefined" ? input.maxlength : '22'
}

xphone.prototype.getElement = function() {
	required_output = '';
	required_tag = '';

	if(this.required) {
		if(this.label != "") {
			required_output = '<sup class="text-danger">*</sup>';
		} else {
			required_output = ''
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}

	if(this.label != "") {
		this.label = "<label>"+this.label+required_output+"</label>";	
	}

	if(this.maxlength != "") {
		this.maxlength = "maxlength="+this.maxlength;
	}

	if(this.mask != "") {
		this.mask = "data-masked-input="+this.mask;
	}

	output = `<div class="form-group">
			  ${this.label}
			  <div class="input-group">
			  <input type="text" class="form-control" id="${this.id}" name="post[${this.name}]" value="${this.value}" placeholder="${this.placeholder}" data-parsley-type="number" ${required_tag} ${this.mask} ${this.maxlength} />
			  </div>`;

	return output;
}