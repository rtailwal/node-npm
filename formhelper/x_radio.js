module.exports = xradio;

function xradio (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.checked = typeof input.checked !== "undefined" ? input.checked : false,
	this.checked_if = typeof input.checked_if !== "undefined" ? input.checked_if : '',
	this.align = typeof input.align !== "undefined" ? input.align : false,
	this.inline = typeof input.inline !== "undefined" ? input.inline : false,
	this.include_false_as_hidden = typeof input.include_false_as_hidden !== "undefined" ? input.include_false_as_hidden : false,
	this.form_group = typeof input.form_group !== "undefined" ? input.form_group : true
}

xradio.prototype.getElement = function() {
	checked_output = "";
	align_class = "";
	inline_class = "";
	form_group_start = "";
	form_group_end = "";
	include_false_as_hidden_element = "";

	if(this.checked == this.checked_if) {
		checked_output='checked="checked"';
	}

	if(this.checked_if != "") {
		this.checked = false;
	}

	if(this.checked == true) {
		checked_output='checked="checked"';
	}

	if(this.align == "left") {
		align_class='pull-left';
	}

	if(this.align == "right") {
		align_class='pull-right';
	}

	if(this.inline == true) {
		inline_class ='inline';
	}

	if(this.form_group == true) {
		form_group_start = '<div class="form-group">';
		form_group_end = '</div>';
	}

	if(this.include_false_as_hidden == true) {
		include_false_as_hidden_element = `<input type="hidden" id="${this.id}" name="post[${this.name}]" value="0">`;
	}

	output = `${form_group_start}
			  <div class="radio">
			  <label class="${align_class} ${inline_class}">
			  ${include_false_as_hidden_element}
			  <input type="radio" id="${this.id}" name="post[${this.name}]" value="${this.value}" ${checked_output} data-toggle="radio"/>
			  ${this.label}</label>
			  </div>
			  ${form_group_end}`;

	return output;
}