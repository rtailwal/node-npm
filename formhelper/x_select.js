module.exports = xselect;

function xselect (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.name_col = typeof input.name_col !== "undefined" ? input.name_col : this.name,
	this.value_col = typeof input.value_col !== "undefined" ? input.value_col : this.id,
	this.css = typeof input.css !== "undefined" ? input.css : "selectpicker",
	this.menu_style = typeof input.menu_style !== "undefined" ? input.menu_style : "dropdown-blue",
	this.style = typeof input.style !== "undefined" ? input.style : "btn-default btn-block",
	this.option_data = typeof input.option_data !== "undefined" ? input.option_data : "",
	this.option_name = typeof input.option_name !== "undefined" ? input.option_name : this.name,
	this.option_value = typeof input.option_value !== "undefined" ? input.option_value : this.id,
	this.selected_value = typeof input.selected_value !== "undefined" ? input.selected_value : "",
	this.title = typeof input.title !== "undefined" ? input.title : "",
	this.multiple = typeof input.multiple !== "undefined" ? input.multiple : false,
	this.add_none = typeof input.add_none !== "undefined" ? input.add_none : false,
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.menu_size = typeof input.menu_size !== "undefined" ? input.menu_size : 0,
	this.live_search = typeof input.live_search !== "undefined" ? input.live_search : false
}

xselect.prototype.getElement = function() {

	required_output = "";
    required_tag = "";
	options_html = "";
	show_multiple = "";
	add_none_option = "";
	live_search_html = "";
	multiple_name = "";

	if(this.required) {
		if(this.label != "") {			
			required_output = '<sup class="text-danger">*</sup>';
		} else {
			required_output = ''
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}

	if(this.multiple == true) {
		multiple_name = "[]";
	}

	this.option_data.forEach(function(a){
		selected = "";
		if(this.selected_value = a.id) {
			selected = "selected='selected'";
		}

		if(this.name_col.constructor != Array) {
			options_html = options_html +"<option value="+a[value_col.to_sym]+selected+">"+a[name_col.to_sym]+"</option>";
		} else {
			options_html = options_html + "<option value="+a[value_col.to_sym] +selected+">";
			this.name_col.forEach(function(b){
				options_html = options_html + a[b.to_sym];
			})
			options_html = options_html + "</option>";
		}
	})

	if(this.option_data.count == 0 || this.option_data == null) {
		options_html = options_html + "<option value=''>No available selections</option>";
	}

	if(this.add_none == true) {
		add_none_option = "<option value=''>Select One</option>";
	}

	if(this.menu_size == 0) {
		menu_size_html = "";
	} else {
		menu_size_html = "data-size="+this.menu_size;
	}

	if(this.live_search == true){
		live_search_html = "data-live-search='true'";
	}


	output =   `<div class="form-group">
				<label>${this.label}${required_output}</label>
				<select id="${this.id}" name="post[${this.name}]${multiple_name}" class="${this.css}" data-title="${this.title}" title="${this.title}" data-style="${this.style}" data-menu-style="${this.menu_style}" ${show_multiple}" ${required_tag} ${menu_size_html} data-container=".content" ${live_search_html}>
				${add_none_option}
				${options_html}
				</select>
				</div>`;

	return output;
}