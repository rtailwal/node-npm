module.exports = xsqft;

function xsqft (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.mask = typeof input.mask !== "undefined" ? input.mask : "",
	this.maxlength = typeof input.maxlength !== "undefined" ? input.maxlength : ""
}

xsqft.prototype.getElement = function() {

	required_output = "";
    required_tag = "";
    if(this.id == ""){
    	this.id = this.name;
    }

	if(this.required) {
		if(this.label != "") {			
			required_output = '<sup class="text-danger">*</sup>';
		} else {
			required_output = ''
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}

	if(this.label != "") {
		this.label = "<label>"+this.label+"</label>";	
	} 

	if(this.maxlength != "") {
		this.maxlength = "maxlength="+this.maxlength;
	}

	if(this.mask != "") {
		this.mask = "data-masked-input="+this.mask;
	}

	output = `<div class="form-group">
			  ${this.label}${required_output}
			  <div class="input-group">
			  <input type="text" class="form-control" id="${this.id}" name="post[${this.name}]" value="${this.value}" placeholder="${this.placeholder}" data-parsley-type="number" ${required_tag} ${this.mask} ${this.maxlength} />
			  <span class="input-group-addon">sqft</span>
			  </div>
			  </div>`;

	return output;
}