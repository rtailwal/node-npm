module.exports = xssn;

function xssn (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.mask = typeof input.mask !== "undefined" ? input.mask : "999-99-9999",
	this.maxlength = typeof input.maxlength !== "undefined" ? input.maxlength : ""
}

xssn.prototype.getElement = function() {

	required_output = "";
    required_tag = "";
    if(this.id == ""){
    	this.id = this.name;
    }

	if(this.required) {
		if(this.label != "") {			
			required_output = '<sup class="text-danger">*</sup>';
		} else {
			required_output = ''
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}

	if(this.label != "") {
		this.label = "<label>"+this.label+required_output+"</label>";	
	}

	if(this.mask != "") {
		this.mask = "data-masked-input="+this.mask;
	}

	output = `<div class="form-group">
			  ${this.label}
			  <input type="text" class="form-control ssn" id="${this.id}" name="post[${this.name}]" value="${this.value}" ${required_tag}  data-parsley-minlength="11" placeholder="${this.placeholder}" ${this.mask} />
			  </div>`;

	return output;
}