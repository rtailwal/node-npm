module.exports = xstate;

function xstate (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.mask = typeof input.mask !== "undefined" ? input.mask : "aa",
	this.maxlength = typeof input.maxlength !== "undefined" ? input.maxlength : "2"
}

xstate.prototype.getElement = function() {

	required_output = "";
    required_tag = "";
    if(this.id == ""){
    	this.id = this.name;
    }

	if(this.required) {
		if(this.label != "") {			
			required_output = '<sup class="text-danger">*</sup>';
		} else {
			required_output = ''
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}

	if(this.label != "") {
		this.label = "<label>"+this.label+required_output+"</label>";	
	}

	if(this.mask != "") {
		this.mask = "data-masked-input="+this.mask;
	}

	if(this.maxlength != "") {
		this.maxlength = "maxlength="+this.maxlength;
	}

	output = `<div class="form-group">
			  ${this.label}
			  <input type="text" class="form-control state" id="${this.id}" name="post[${this.name}]" value="${this.value}" ${required_tag}  data-parsley-pattern="^[A-Za-z]*$" data-parsley-minlength="2" placeholder="${this.placeholder}"  ${this.maxlength} />
			  </div>`;

	return output;
}