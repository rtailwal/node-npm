module.exports = xsubmit;

function xsubmit (input){
	this.css = typeof input.css !== "undefined" ? input.css : 'btn-primary',
	this.icon = typeof input.icon !== "undefined" ? input.icon : '',
	this.size = typeof input.size !== "undefined" ? input.size : '',
	this.label = typeof input.label !== "undefined" ? input.label : 'Submit',
	this.wrapper_css = typeof input.wrapper_css !== "undefined" ? input.wrapper_css : "pull-right"
}

xsubmit.prototype.getElement = function() {

	required_output = "";
    required_tag = "";
    if(this.icon != ""){
    	this.icon = "<i class='fa #{icon}'></i> "
    }

	output = `<div class="form-group form-actions ${this.wrapper_css}">
			  <button type="submit" class="btn ${this.css} ${this.size}">${this.icon}${this.label}</button>
			  </div>
			  <div class="clearfix"></div>`;

	return output;
}