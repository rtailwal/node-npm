module.exports = xsurveyradio;

function xsurveyradio (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : ''
}

xsurveyradio.prototype.getElement = function() {

	output =   `<label class="radio radio-inline">
				<input type="radio" id="${this.id}" name="${this.name}" data-toggle="radio" value="1">
				</label>
				<label class="radio radio-inline">
				<input type="radio" id="${this.id}" name="${this.name}" data-toggle="radio" value="2">
				</label>
				<label class="radio radio-inline">
				<input type="radio" id="${this.id}" name="${this.name}" data-toggle="radio" value="3">
				</label>
				<label class="radio radio-inline">
				<input type="radio" id="${this.id}" name="${this.name}" data-toggle="radio" value="4">
				</label>
				<label class="radio radio-inline">
				<input type="radio" id="${this.id}" name="${this.name}" data-toggle="radio" value="5">
				</label>
				<label class="radio radio-inline">
				<input type="radio" id="${this.id}" name="${this.name}" data-toggle="radio" value="6">
				</label>
				<label class="radio radio-inline">
				<input type="radio" id="${this.id}" name="${this.name}" data-toggle="radio" value="7">
				</label>`;

	return output;
}