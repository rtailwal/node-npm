module.exports = xswitch;

function xswitch (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.checked = typeof input.checked !== "undefined" ? input.checked : false
}

xswitch.prototype.getElement = function() {

	required_output = "";
	checked_output = "";

	if(this.checked == true) {
		checked_output='checked="checked"';	
	}

	output = `<div class="form-group">
			  <label>${this.label}</label><br>
			  <div class="switch"
			  data-on-label="<i class='fa fa-check'></i>"
			  data-off-label="<i class='fa fa-times'></i>">
			  <input type="hidden" id="${this.id}" name="post[${this.name}]" value="0">
			  <input type="checkbox" id="${this.id}" name="post[${this.name}]" value="1" ${checked_output}/>
			  </div>
			  </div>`;

	return output;
}