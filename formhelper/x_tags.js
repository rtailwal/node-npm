module.exports = xtags;

function xtags (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : ''
}

xtags.prototype.getElement = function() {

	required_output = '';
	required_tag = '';

	if(this.id == ""){
    	this.id = this.name;
    }

    if(this.required) {
		if(this.label != "") {			
			required_output = '<sup class="text-danger">*</sup>';
		} else {
			required_output = ''
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}

	if(this.label != "") {
		this.label = "<label>"+this.label+"</label>";	
	}

	output = `<div class="form-group">
			  ${this.label}${required_output}
			  <input type="text" class="form-control tagsinput typeahead tag-azure tag-fill" id="${this.id}" name="post[${this.name}]" value="${this.value}" placeholder="${this.placeholder}" ${required_tag}/>
			  </div>`;

	return output;
}