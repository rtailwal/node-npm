module.exports = xtextarea;

function xtextarea (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.maxlength = typeof input.maxlength !== "undefined" ? input.maxlength : ''
}

xtextarea.prototype.getElement = function() {

	required_output = '';
	required_tag = '';

    if(this.required) {
		if(this.label != "") {			
			required_output = '<sup class="text-danger">*</sup>';
		} else {
			required_output = ''
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}

	if(this.label != "") {
		this.label = "<label for="+this.id+">"+this.label+"</label>";	
	}

	if(this.maxlength != "") {
		this.maxlength = "maxlength="+this.maxlength;
	}

	output = `<div class="form-group">
			  ${this.label}${required_output}
			  <textarea name="post[${this.name}]" id="${this.id}" rows="4" class="form-control" placeholder="${this.placeholder}" ${this.maxlength} ${required_tag}>${this.value}</textarea>
			  </div>`;

	return output;
}