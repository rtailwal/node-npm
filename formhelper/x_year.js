module.exports = xyear;

function xyear (input){
}

xyear.prototype.getElement = function() {

	var date = new Date();
	current_year = date.getFullYear();

	output = `<select id="selected_year" name="selected_year" class="selectpicker" data-style="btn-primary btn" data-width="auto">
			  <option selected="selected" value="${current_year}">${current_year}</option>
			  <option value="${current_year - 1}">${current_year - 1}</option>
			  <option value="${current_year - 2}">${current_year - 2}</option>
			  <option value="${current_year - 3}">${current_year - 3}</option>
			  <option value="${current_year - 4}">${current_year - 4}</option>
			  <option value="${current_year - 5}">${current_year - 5}</option>
			  <option value="${current_year - 6}">${current_year - 6}</option>
			  <option value="${current_year - 7}">${current_year - 7}</option>
			  </select>`;

	return output;
}