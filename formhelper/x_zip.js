module.exports = xzip;

function xzip (input){
	this.id = typeof input.id !== "undefined" ? input.id : '',
	this.name = typeof input.name !== "undefined" ? input.name : '',
	this.value = typeof input.value !== "undefined" ? input.value : '',
	this.label = typeof input.label !== "undefined" ? input.label : '',
	this.required = typeof input.required !== "undefined" ? input.required : false,
	this.placeholder = typeof input.placeholder !== "undefined" ? input.placeholder : '',
	this.mask = typeof input.mask !== "undefined" ? input.mask : '99999',
	this.maxlength = typeof input.maxlength !== "undefined" ? input.maxlength : '5'
}

xzip.prototype.getElement = function() {
	required_output = '';
	required_tag = '';

	if(this.required) {
		if(this.label != "") {			
			required_output = '<sup class="text-danger">*</sup>';
		} else {
			required_output = ''
		}
		required_tag = 'required="required"';
	}else {
		required_output = "";
    	required_tag = "";
	}

	if(this.label != "") {
		this.label = "<label>"+this.label+required_output+"</label>";	
	} 

	if(this.maxlength != "") {
		this.maxlength = "maxlength="+this.maxlength;
	}

	if(this.mask != "") {
		this.mask = "data-masked-input="+this.mask;
	}

	output = `<div class="form-group">
			  ${this.label}
			  <div class="input-group">
			  <input type="text" class="form-control zip" id="${this.id}" name="post[${this.name}]" value="${this.value}" ${required_tag}  placeholder="${this.placeholder}" ${this.mask} data-parsley-range="[10000,99999]"  />
			  </div>`;

	return output;
}