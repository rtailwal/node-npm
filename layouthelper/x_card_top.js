module.exports = xcardtop;

function xcardtop (input){
	this.title = typeof input.title !== "undefined" ? input.title : '',
	this.subtitle = typeof input.subtitle !== "undefined" ? input.subtitle : '',
	this.card_class = typeof input.card_class !== "undefined" ? input.card_class : '',
	this.header_css = typeof input.header_css !== "undefined" ? input.header_css : '',
	this.buttons = typeof input.buttons !== "undefined" ? input.buttons : "",
	this.modal_cog_url = typeof input.modal_cog_url !== "undefined" ? input.modal_cog_url : ''
}

xcardtop.prototype.getElement = function() {

	modal_cog_html = "";

	if(this. modal_cog_url != "") {
		modal_cog_html = "<a href="+this.modal_cog_url+" class='btn btn-button btn-sm' data-toggle='modal'><i class='fa fa-cog'></i></a>";
	}

	if(this.title != "") {
		this.title = `<div class="header ${this.header_css}">
					<h4 class="title pull-left">${this.title}</h4>
					<div class="pull-right title_right">${this.buttons} ${modal_cog_html}</div>
					<div class="clearfix"></div>
					<p class="category">${this.subtitle}</p>
					</div>`;
	}

	output = `<div class="card ${this.card_class}">
			  ${this.title}
			  <div class="content">`;

	return output;
}