module.exports = xtabs;

function xtabs (input){
	this.active = typeof input.active !== "undefined" ? input.active : '',
	this.tabs = typeof input.tabs !== "undefined" ? input.tabs : [] 
}

xtabs.prototype.getElement = function() {

	tab_builder = "";

	this.tabs.forEach(function(opt){
		s = "";
		if(opt.name == this.active) {
			active_s = "active";
		} else {
			active_s = "";
		}
		s = `<li class="${active_s}">
			<a href="${opt.url}"><i class="fa ${opt.icon}"></i> ${opt.name}</a>
			</li>`;

		tab_builder   = tab_builder + s;
	})

	output = `<div class="tabs_wrapper">
			  <ul class="nav nav-tabs" role="tablist">
			  ${tab_builder}
			  </ul>
			  </div>`;

	return output;
}