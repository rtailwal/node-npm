var xformattime = require('../datehelper/x_format_time.js');

module.exports = {
	x_format_time : x_format_time
}

function x_format_time(input)
{
	return new xformattime(input).getElement();
}