var formHelper = require('./platformx-form-helper');
var layoutHelper = require('./platformx-layout-helper');
var dateHelper = require('./platformx-date-helper');

module.exports = function() {
	
	return function(req,res,next){
		
		res.locals.x_checkbox = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_checkbox(input);
		}
		
		res.locals.x_credit_card = function(input) {
			return formHelper.x_credit_card(input);
		}

		res.locals.x_cvc = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_cvc(input);
		}

		res.locals.x_date = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_date(input);
		}
		
		res.locals.x_datetime = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_date(input);
		}

		res.locals.x_ein = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_ein(input);
		}

		res.locals.x_email = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_email(input);
		}

		res.locals.x_exp_month = function(input) {
			return formHelper.x_exp_month(input);
		}
	   	
	   	res.locals.x_exp_year = function(input) {
			return formHelper.x_exp_year(input);
		}

		res.locals.x_file = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_file(input);
		}

		res.locals.x_hidden = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_hidden(input);
		}

		res.locals.x_html = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_html(input);
		}

		res.locals.x_image_upload = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_image_upload(input);
		}

		res.locals.x_input = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_input(input);
		}

		res.locals.x_money = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_money(input);
		}

		res.locals.x_number = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_number(input);
		}

		res.locals.x_password = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_password(input);
		}

		res.locals.x_percentage = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_percentage(input);
		}

		res.locals.x_phone = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_phone(input);
		}

		res.locals.x_radio = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_radio(input);
		}

		res.locals.x_select = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_select(input);
		}

		res.locals.x_sqft = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_sqft(input);
		}

		res.locals.x_ssn = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_ssn(input);
		}

		res.locals.x_state = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_state(input);
		}

		res.locals.x_submit = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_submit(input);
		}

		res.locals.x_survey_radio = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_survey_radio(input);
		}

		res.locals.x_switch = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_switch(input);
		}

		res.locals.x_tags = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_tags(input);
		}

		res.locals.x_textarea = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_textarea(input);
		}

		res.locals.x_time = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_time(input);
		}

		res.locals.x_typeahead = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_typeahead(input);
		}

		res.locals.x_url = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_url(input);
		}

		res.locals.x_year = function(input) {
			return formHelper.x_year(input);
		}

		res.locals.x_zip = function(input) {
			if(input && (input.constructor == Object))
				return formHelper.x_zip(input);
		}

		res.locals.x_card_bottom = function(input) {
			return layoutHelper.x_card_bottom(input);
		}

		res.locals.x_card_top = function(input) {
			if(input && (input.constructor == Object))
				return layoutHelper.x_card_top(input);
		}

		res.locals.x_tabs = function(input) {
			if(input && (input.constructor == Object))
				return layoutHelper.x_tabs(input);
		}

		res.locals.x_format_time = function(input) {
			if(input && (input.constructor == Object))
				return dateHelper.x_format_time(input);
		}

	   	next();
	}

}