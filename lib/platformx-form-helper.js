var xCheckbox = require('../formhelper/x_checkbox.js');
var xCreditCard = require('../formhelper/x_credit_card.js');
var xcvc = require('../formhelper/x_cvc.js');
var xdate = require('../formhelper/x_date.js');
var xein = require('../formhelper/x_ein.js');
var xemail = require('../formhelper/x_email.js');
var xempmonth = require('../formhelper/x_exp_month.js');
var xexpyear = require('../formhelper/x_exp_year.js');
var xfile = require('../formhelper/x_file.js');
var xhidden = require('../formhelper/x_hidden.js');
var xhtml = require('../formhelper/x_html.js');
var ximageupload = require('../formhelper/x_image_upload.js');
var xinput = require('../formhelper/x_input.js');
var xmoney = require('../formhelper/x_money.js');
var xnumber = require('../formhelper/x_number.js');
var xpassword = require('../formhelper/x_password.js');
var xpercentage = require('../formhelper/x_percentage.js');
var xphone = require('../formhelper/x_phone.js');
var xradio = require('../formhelper/x_radio.js');
var xselect = require('../formhelper/x_select.js');
var xsqft = require('../formhelper/x_sqft.js');
var xssn = require('../formhelper/x_ssn.js');
var xstate = require('../formhelper/x_state.js');
var xsubmit = require('../formhelper/x_submit.js');
var xsurveyradio = require('../formhelper/x_survey_radio.js');
var xswitch = require('../formhelper/x_switch.js');
var xtags = require('../formhelper/x_tags.js');
var xtextarea = require('../formhelper/x_textarea.js');
var xtime = require('../formhelper/x_time.js');
var xtypeahead = require('../formhelper/x_typeahead.js');
var xurl = require('../formhelper/x_url.js');
var xyear = require('../formhelper/x_year.js');
var xzip = require('../formhelper/x_zip.js');

module.exports = {
	x_checkbox : x_checkbox,
	x_credit_card : x_credit_card,
	x_cvc : x_cvc,
	x_date : x_date,
	x_ein : x_ein,
	x_email : x_email,
	x_exp_month : x_exp_month,
	x_exp_year : x_exp_year,
	x_file : x_file,
	x_hidden : x_hidden,
	x_html : x_html,
	x_image_upload : x_image_upload,
	x_input : x_input,
	x_money : x_money,
	x_number : x_number,
	x_password : x_password,
	x_percentage : x_percentage,
	x_phone : x_phone,
	x_radio : x_radio,
	x_select : x_select,
	x_sqft : x_sqft,
	x_ssn : x_ssn,
	x_state : x_state,
	x_submit : x_submit,
	x_survey_radio : x_survey_radio,
	x_switch : x_switch,
	x_tags : x_tags,
	x_textarea : x_textarea,
	x_time : x_time,
	x_typeahead : x_typeahead,
	x_url : x_url,
	x_year : x_year,
	x_zip : x_zip
}

function x_checkbox(input)
{
	return new xCheckbox(input).getElement();
}

function x_credit_card(input)
{
	return new xCreditCard(input).getElement();
}

function x_cvc(input)
{
	return new xcvc(input).getElement();
}

function x_date(input)
{
	return new xdate(input).getElement();
}

function x_ein(input)
{
	return new xein(input).getElement();
}

function x_email(input)
{
	return new xemail(input).getElement();
}

function x_exp_month(input)
{
	return new xempmonth(input).getElement();
}

function x_exp_year(input)
{
	return new xexpyear(input).getElement();
}

function x_file(input)
{
	return new xfile(input).getElement();
}

function x_hidden(input)
{
	return new xhidden(input).getElement();
}

function x_html(input)
{
	return new xhtml(input).getElement();
}

function x_image_upload(input)
{
	return new ximageupload(input).getElement();
}

function x_input(input)
{
	return new xinput(input).getElement();
}

function x_money(input)
{
	return new xmoney(input).getElement();
}

function x_number(input)
{
	return new xnumber(input).getElement();
}

function x_password(input)
{
	return new xpassword(input).getElement();
}

function x_percentage(input)
{
	return new xpercentage(input).getElement();
}

function x_phone(input)
{
	return new xphone(input).getElement();
}

function x_radio(input)
{
	return new xradio(input).getElement();
}

function x_select(input)
{
	return new xselect(input).getElement();
}

function x_sqft(input)
{
	return new xsqft(input).getElement();
}

function x_ssn(input)
{
	return new xssn(input).getElement();
}

function x_state(input)
{
	return new xstate(input).getElement();
}

function x_submit(input)
{
	return new xsubmit(input).getElement();
}

function x_survey_radio(input)
{
	return new xsurveyradio(input).getElement();
}

function x_switch(input)
{
	return new xswitch(input).getElement();
}

function x_tags(input)
{
	return new xtags(input).getElement();
}

function x_textarea(input)
{
	return new xtextarea(input).getElement();
}

function x_time(input)
{
	return new xtime(input).getElement();
}

function x_typeahead(input)
{
	return new xtypeahead(input).getElement();
}

function x_url(input)
{
	return new xurl(input).getElement();
}

function x_year(input)
{
	return new xyear(input).getElement();
}

function x_zip(input)
{
	return new xzip(input).getElement();
}