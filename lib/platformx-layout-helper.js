var xcardbottom = require('../layouthelper/x_card_bottom.js');
var xcardtop = require('../layouthelper/x_card_top.js');
var xtabs = require('../layouthelper/x_tabs.js');

module.exports = {
	x_card_bottom : x_card_bottom,
	x_card_top : x_card_top,
	x_tabs : x_tabs
}

function x_card_bottom(input)
{
	return new xcardbottom(input).getElement();
}

function x_card_top(input)
{
	return new xcardtop(input).getElement();
}

function x_tabs(input)
{
	return new xtabs(input).getElement();
}