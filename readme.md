# Usage
add module into your main file: 
var platformxHelper = require('platformx');
app.use(platformxHelper());


## install ejs module to render view by running command
npm install ejs

## create a folder "views" in view to render view on page request

Example:- create a home.ejs file in views folder
## Add code in root js file 
app.get('/', function(request, response) {
 response.render("home");
})

### Form Hepler function
	* x_checkbox({id: "", name: "", value: "", label: "", checked: false, switch: false, align: false, inline: false, form_group: true, required: false})
     View helper to generate check box

	* x_checkbox_string({id: "", name: "", value: "", label: "", checked: false, switch: false, align: false, inline: false, form_group: true, required: false})
     Check box helper

	* x_credit_card({id: "", name: "", value: "", label: "", required: false, placeholder: "", maxlength: "16", mask: ""})
     Credit card input helper

	* x_cvc({id: "", name: "", value: "", label: "", required: false, placeholder: "", maxlength: "4", mask: "999999999999999999"})
     CVC input helper

	* x_date({id: "", name: "", value: "", label: "", required: false, placeholder: ""})
     Date input helper

	* x_datetime({id: "", name: "", value: "", label: "", required: false, placeholder: ""})
   	 Date-time input helper

	* x_ein({id: "", name: "", value: "", label: "", required: false, placeholder: "", mask: "99-9999999", maxlength: "10"})
     EIN input helper

	* x_email({id: "", name: "", value: "", label: "", required: false, placeholder: ""})
	 Email input helper

	* x_exp_month()
	 Month Drop down

	* x_exp_year()
	 Year Drop down (expiry)

	* x_file({id: "", name: "", value: "", label: "", required: false, placeholder: "", css: ""})
	 File input helper

	* x_hidden({id: "", name: "", value: ""})
	 Hidden input helper

	* x_html({id: "", name: "", value: "", label: "", help: "", required: false})
	 Summernote

	* x_image_upload({id: "input1", name: "input1", value: "", label: "Input1", required: false, size: "1M", height: "", default: ""})
	 Image Upload

	* x_input({id: "", name: "", value: "", label: "", required: false, placeholder: "", mask: "", maxlength: "", css: ""})
	 Text input helper

	* x_money({id: "", name: "", value: "", label: "", required: false, placeholder: "", mask: "", maxlength: ""})
	 Money input helper

	* x_number({id: "", name: "", value: "", label: "", required: false, placeholder: "", mask: "", maxlength: ""})
	 Number input helper

	* x_password({id: "", name: "", value: "", label: "", required: false, placeholder: ""})
	 Password input helper

	* x_percentage({id: "", name: "", value: "", label: "", required: false, placeholder: "", mask: "", maxlength: ""})
	 Percentage input helper

	* x_phone({id: "", name: "", value: "", label: "", required: false, placeholder: "", mask: "(999) 999-9999 x99999", maxlength: "22"})
	 Phone input helper

	* x_radio({id: "", name: "", value: "1", label: "", checked: false, checked_if: "", align: false, inline: false, include_false_as_hidden: false, form_group: true})
	 Radio button view helper

	* x_select({id: "", name: "", value: "", name_col: "name", value_col: "id", label: "", css: "selectpicker", menu_style: "dropdown-blue", style: "btn-default btn-block", option_data: "", option_name: "name", option_value: "id", selected_value: "", title: "", multiple: false, add_none: false, required: false, menu_size: 0, live_search: false})
	 Select Dropdown

	* x_sqft({id: "", name: "", value: "", label: "", required: false, placeholder: "", mask: "", maxlength: ""})
	 Sq Ft input helper

	* x_ssn(id: "", name: "", value: "", label: "", required: false, placeholder: "", mask: "999-99-9999")
	 SSN input helper

	* x_state(id: "", name: "", value: "", label: "", required: false, placeholder: "", mask: "aa", maxlength: "2")
	 State input helper

	* x_submit({label: "Submit", css: "btn-primary", icon: "", size: "", wrapper_css: "pull-right"})
	 Submit Helper

	* x_survey_radio({id: "", name: ""})
	 Rating Radios

	* x_switch({id: "", name: "", value: "", label: "", checked: false})
	 Switch

	* x_tags({id: "", name: "", value: "", label: "", required: false, placeholder: ""})
	 Tags

	* x_textarea({id: "", name: "", value: "", label: "", placeholder: "", required: false, maxlength: ""})
	 Text Area Helper

	* x_time({id: "", name: "", value: "", label: "", required: false, placeholder: ""})
	 Time input helper

	* x_typeahead({id: "", name: "", value: "", label: "", required: false, placeholder: "", css: ""})
	 Typeahead

	* x_url({id: "", name: "", value: "", label: "", required: false, placeholder: ""})
	 URL input helper

	* x_year()
	   Year Drop down

	* x_zip({id: "", name: "", value: "", label: "", required: false, placeholder: "", mask: "99999", maxlength: "5"})
	 Zip input helper

### Layout Hepler function

	* x_card_top({title: "", subtitle: "", card_class: "", header_css: "", buttons: "", modal_cog_url: ""})
	 Card (top) view helper

	* x_card_bottom()
	   Card (bottom) view helper

	* x_tabs({active: "", tabs: array})
	   Student Tabs Nav

### Date Hepler function
	* x_format_time({time:""})
	   Formats the time